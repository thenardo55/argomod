//
//  ReportImageCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 13/11/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class ReportImageCell: UICollectionViewCell {
    
    @IBOutlet weak var reportImageImg: UIImageView!
    
}
