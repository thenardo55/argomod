//
//  ReportView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 11/11/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Firebase

class ReportView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, SSRadioButtonControllerDelegate {
    
    @IBOutlet weak var reportScrollView: UIScrollView!
    @IBOutlet weak var reportMoreTextVw: UITextView!
    @IBOutlet weak var reportImageCollection: UICollectionView!
    @IBOutlet weak var txtVwCount: UILabel!
    
    var reportOptionBtnController: SSRadioButtonsController?
    @IBOutlet weak var reportOption01: SSRadioButton!
    @IBOutlet weak var reportOption02: SSRadioButton!
    @IBOutlet weak var reportOption03: SSRadioButton!
    
    var loadingView: UIView?
    
    // variable controller utk image picker
    var imagePicker = UIImagePickerController()
    // variable array utk image report
    var reportImg: [ReportImgInfo] = []
    
    var db: Firestore!
    var storage: Storage!
    
    // MARK: - Initial Func
    
    override func viewWillAppear(_ animated: Bool) {
        registerForKeyboardNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()
        storage = Storage.storage()
        
        reportMoreTextVw.delegate = self
        
        reportImageCollection.delegate = self
        reportImageCollection.dataSource = self
        
        let dismissMoreInputTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector(("dismissMoreInput")))
        dismissMoreInputTap.cancelsTouchesInView = false
        view.addGestureRecognizer(dismissMoreInputTap)
        
        let initialRp = ReportImgInfo(img: UIImage(named: "Add Photo Icon")!, filename: "placeholder")
        reportImg.append(initialRp)
        
        reportOptionBtnController = SSRadioButtonsController(buttons: reportOption01, reportOption02, reportOption03)
        reportOptionBtnController?.delegate = self
        reportOptionBtnController?.shouldLetDeSelect = false
        
        if traitCollection.userInterfaceStyle == .dark {
            reportOption01.setTitleColor(.white, for: .normal)
            reportOption02.setTitleColor(.white, for: .normal)
            reportOption03.setTitleColor(.white, for: .normal)
        } else {
            reportOption01.setTitleColor(.black, for: .normal)
            reportOption02.setTitleColor(.black, for: .normal)
            reportOption03.setTitleColor(.black, for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        deregisterFromKeyboardNotifications()
    }
    
    // MARK: - Keyboard Function
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        let userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.reportScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        reportScrollView.contentInset = contentInset
        
        self.view.frame.origin.y = -150
    }

    @objc func keyboardWillBeHidden(notification: NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        reportScrollView.contentInset = contentInset
        
        self.view.frame.origin.y = 0
    }
    
    // MARK: - Delegate Text View
    
    func textViewDidChange(_ textView: UITextView) {
        txtVwCount.text = "\(reportMoreTextVw.text.count) / 140"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currText = textView.text ?? ""
        
        guard let stringRange = Range(range, in: currText) else { return false }
        
        let newText = currText.replacingCharacters(in: stringRange, with: text)
        
        return newText.count <= 140
    }
    
    @objc func dismissMoreInput() {
        view.endEditing(true)
    }
    
    @IBAction func dismissReport(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func didSelectButton(selectedButton: UIButton?) {
        //
    }
    
    // MARK: - Collection Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reportImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reportPhotoItem", for: indexPath) as! ReportImageCell
        
        let reportItem = reportImg[indexPath.row]
        
        cell.reportImageImg.image = reportItem.img
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.row > 0 {
            print("Terpilih di index: \(indexPath.row)")
            deleteImageReport(index: indexPath.row)
        } else {
            openImagePicker()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    // MARK: - Image Picker Functions
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        
        guard let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }
        //print(imageURL.lastPathComponent)
        //print(imageURL.pathExtension)
        
        guard let image = info[.originalImage] as? UIImage else { return }
        let newRp = ReportImgInfo(img: image, filename: imageURL.lastPathComponent)
        reportImg.append(newRp)
        reportImageCollection.reloadData()
    }
    
    func openImagePicker() {
        if reportImg.count < 4 {
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                imagePicker.delegate = self
                imagePicker.sourceType = .savedPhotosAlbum
                imagePicker.allowsEditing = false
                
                present(imagePicker, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Maximum image reached", message: "You can only attach up to 3 images.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func deleteImageReport(index: Int) {
        let alert = UIAlertController(title: "Delete Confirmation", message: "Do you want to delete this image?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { action in
            self.reportImg.remove(at: index)
            self.reportImageCollection.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    // MARK: - Submit Functions
    
    @IBAction func submitReport(_ sender: UIButton) {
        
        if reportOptionBtnController?.selectedButton() != nil {
            sendReport()
        } else {
            print("Harap pilih tipe problem")
        }
    }
    
    func uploadImage() {
        if reportImg.count > 0 {
            for i in 1..<reportImg.count {
                print(reportImg[i].filename)
                
                var imgData = Data()
                imgData = reportImg[i].img.pngData()!

                let storageRef = storage.reference()
                let reportRef = storageRef.child("reports/freshthumb-report-\(getReportDateTime().codedDate)/\(reportImg[i].filename)")

                reportRef.putData(imgData, metadata: nil) { (metadata, error) in
                    //guard let metadara = metadata else { return }

                    if let error = error {
                        print("Error while uploading | ERR: \(error)")
                    } else {
                        print("Upload success")
                    }
                }
            }
        }
    }
    
    func sendReport() {
        showLoading(onView: self.view)
        uploadImage()
        db.collection("reports").document("freshthumb-report-\(getReportDateTime().codedDate)").setData([
            "date_time": getReportDateTime().formmatedDate,
            "problemType": getReportProblemType(),
            "problemTellMore": getReportTellMeMore()
        ]) { err in
            if let err = err {
                self.removeLoading()
                print("Error writing document: \(err)")

                let alert = UIAlertController(title: "Error", message: "Something wrong happened when sending the report. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.removeLoading()
                print("Document successfully written!")

                let alert = UIAlertController(title: "Report Sent!", message: "We've been received your report. Thank you for your feedback.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getReportDateTime() -> (formmatedDate: String, codedDate: String) {
        let date = Date()
        let format = DateFormatter()
        
        format.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let formattedDate = format.string(from: date)
        
        format.dateFormat = "dd_MM_yyyy_HHmmss"
        let codedDate = format.string(from: date)
        
        return (formattedDate, codedDate)
    }
    
    func getReportProblemType() -> String {
        
        let selectedProblem = reportOptionBtnController?.selectedButton()
        let reportType = selectedProblem?.titleLabel?.text
        
        return reportType!
    }
    
    func getReportTellMeMore() -> String {
        
        var reportText = ""
        
        if reportMoreTextVw.text == "Tell us more" {
            reportText = "-"
        } else {
            reportText = reportMoreTextVw.text
        }
        
        return reportText
    }
    
    // MARK: - Loading Activity Indicator
    
    func showLoading(onView: UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        loadingView = spinnerView
    }
    
    func removeLoading() {
        DispatchQueue.main.async {
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
    }
    
}
