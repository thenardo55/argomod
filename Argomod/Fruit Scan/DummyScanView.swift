//
//  DummyScanView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 25/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class DummyScanView: UIViewController {

    @IBOutlet weak var scanImgAnimation: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        animateScanUp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //scanImgAnimation.transform = scanImgAnimation.transform.rotated(by: CGFloat(Double.pi/1))
    }
    
    func animateScanUp() {
        UIView.animate(withDuration: 1.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.scanImgAnimation.frame.origin.y = -50
        }, completion: { (_) -> Void in
            self.scanImgAnimation.transform = self.scanImgAnimation.transform.rotated(by: CGFloat(Double.pi/1))
            self.animateScanDown()
        })
    }
    
    func animateScanDown() {
        UIView.animate(withDuration: 1.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.scanImgAnimation.frame.origin.y = (UIScreen.main.bounds.height + 50)
        }, completion: { (_) -> Void in
            self.scanImgAnimation.transform = self.scanImgAnimation.transform.rotated(by: CGFloat(Double.pi/1))
            self.animateScanUp()
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
