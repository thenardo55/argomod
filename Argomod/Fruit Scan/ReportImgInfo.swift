//
//  ReportImgInfo.swift
//  Argomod
//
//  Created by Thenardo Ardo on 13/11/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Foundation

class ReportImgInfo {
    var img: UIImage
    var filename: String
    
    init(img: UIImage, filename: String) {
        self.img = img
        self.filename = filename
    }
}
