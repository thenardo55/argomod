//
//  SeeAllCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 13/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class SeeAllCell: UICollectionViewCell {
    
    @IBOutlet weak var seeAllFruitImg: UIImageView!
    @IBOutlet weak var seeAllFruitLabel: UILabel!
    
}
