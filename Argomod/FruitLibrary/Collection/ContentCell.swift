
//
//  ContentCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 26/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class ContentCell: UICollectionViewCell {
    
    @IBOutlet weak var contentImg: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
}
