//
//  FruitCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 06/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class FruitCell: UICollectionViewCell {
    
    @IBOutlet weak var fruitImg: UIImageView!
    @IBOutlet weak var fruitLabel: UILabel!
    
}
