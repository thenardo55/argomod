//
//  FreshthumbNewUIView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 23/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class FreshthumbNewUIView: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var contentContainerView: UIView!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var contentPageCtrl: UIPageControl!
    
    var contents:[Content] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScrollView.delegate = self
        // Do any additional setup after loading the view.
        contents = generateContentScrollView()
        setupContentScrollView(contents: contents)
        contentPageCtrl.numberOfPages = contents.count
        contentPageCtrl.currentPage = 0
    }
    
    func generateContentScrollView() -> [Content] {
        let content1:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content1.contentImg.image = UIImage(named: "contentPlaceholder")
        content1.contentLabel.text = "New Fruits for your breakfast"
        
        let content2:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content2.contentImg.image = UIImage(named: "contentPlaceholder")
        content2.contentLabel.text = "New Veggie for your Salad"
        
        let content3:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content3.contentImg.image = UIImage(named: "contentPlaceholder")
        content3.contentLabel.text = "You will float too..."
        
        return [content1, content2, content3]
    }
    
    func setupContentScrollView(contents:[Content]) {
        contentScrollView.frame = CGRect(x: 0, y: 0, width: contentContainerView.frame.width, height: contentContainerView.frame.height)
        contentScrollView.contentSize = CGSize(width: contentContainerView.frame.width * CGFloat(contents.count), height: contentContainerView.frame.height)
        contentScrollView.isPagingEnabled = true
        
        for i in 0 ..< contents.count {
            contents[i].frame = CGRect(x: contentContainerView.frame.width * CGFloat(i), y: 0, width: contentContainerView.frame.width, height: contentContainerView.frame.height)
            contentScrollView.addSubview(contents[i])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y != 0 {
            scrollView.contentOffset.y = 0
        }
        
        let pageIndex = round(scrollView.contentOffset.x/contentContainerView.frame.width)
        contentPageCtrl.currentPage = Int(pageIndex)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
