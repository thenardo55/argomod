//
//  Content.swift
//  Argomod
//
//  Created by Thenardo Ardo on 23/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class Content: UIView {

    @IBOutlet weak var contentImg: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
