//
//  Item.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Foundation

class Item {
    var name: String
    var img: String
    
    init(name: String, img: String) {
        self.name = name
        self.img = img
    }
}
