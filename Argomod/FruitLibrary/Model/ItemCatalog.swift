//
//  ItemCatalog.swift
//  Argomod
//
//  Created by Thenardo Ardo on 09/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Foundation

class ItemCatalog {
    static let sharedInstance = ItemCatalog()
    let category:[ItemCategories]
    
    init() {
        // fruit
        let f1 = Item(name: "Apple", img: "itemPlaceholder")
        let f2 = Item(name: "Orange", img: "itemPlaceholder")
        let f3 = Item(name: "Pears", img: "itemPlaceholder")
        let f4 = Item(name: "Mango", img: "itemPlaceholder")
        let f5 = Item(name: "Dragon", img: "itemPlaceholder")
        let fruit = ItemCategories(name: "Fruit", item: [f1, f2, f3, f4, f5])
        
        // veggie
        let v1 = Item(name: "Cabbage", img: "itemPlaceholder")
        let v2 = Item(name: "Col", img: "itemPlaceholder")
        let v3 = Item(name: "Celery", img: "itemPlaceholder")
        let v4 = Item(name: "Veggie 1", img: "itemPlaceholder")
        let v5 = Item(name: "Veggie 2", img: "itemPlaceholder")
        let veggie = ItemCategories(name: "Veggie", item: [v1, v2, v3, v4, v5])
        
        // spices
        let s1 = Item(name: "Spices 1", img: "itemPlaceholder")
        let s2 = Item(name: "Spices 2", img: "itemPlaceholder")
        let s3 = Item(name: "Spices 3", img: "itemPlaceholder")
        let s4 = Item(name: "Spices 4", img: "itemPlaceholder")
        let s5 = Item(name: "Spices 5", img: "itemPlaceholder")
        let spices = ItemCategories(name: "Spices", item: [s1, s2, s3, s4, s5])
        
        category = [fruit, veggie, spices]
    }
}
