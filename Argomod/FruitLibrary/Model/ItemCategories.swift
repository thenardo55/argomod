//
//  ItemCategories.swift
//  Argomod
//
//  Created by Thenardo Ardo on 09/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class ItemCategories {
    let name: String
    let item: [Item]
    
    init(name: String, item: [Item]) {
        self.name = name
        self.item = item
    }
}
