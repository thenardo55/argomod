//
//  FreshTabController.swift
//  Argomod
//
//  Created by Thenardo Ardo on 20/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class FreshTabController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

        // Do any additional setup after loading the view.
        self.tabBar.layer.cornerRadius = 20
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = false
    }
    
    // Penampilan tab bar berdasarkan tab yang dipilih
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(tabBarController.selectedIndex)
        if tabBarController.selectedIndex == 0 {
            self.tabBar.layer.cornerRadius = 20
            self.tabBar.layer.masksToBounds = true
            self.tabBar.isTranslucent = false
        } else {
            self.tabBar.layer.cornerRadius = 0
            self.tabBar.layer.masksToBounds = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var customSize = super.sizeThatFits(size)
        customSize.height = 95
        return customSize
    }
    
}
