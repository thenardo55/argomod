//
//  MainFruitCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 06/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class MainFruitCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet private var fruitCollectionView: UICollectionView!
    var category: ItemCategories? = nil {
        didSet {
            fruitCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if category!.item.count < 3 {
            return category!.item.count
        } else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FruitItem", for: indexPath) as! FruitCell
        
        // untuk konfigurasi tampilan
        
        cell.contentView.layer.cornerRadius = 5.0
        cell.contentView.layer.borderWidth = 0.1
        cell.contentView.layer.borderColor = UIColor.black.cgColor
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width:0, height: 2.0)
        cell.layer.shadowRadius = 1.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        //let ex = exerciseName[indexPath.row]
        // untuk populate data
    
        if let category = category {
            let collectionItem = category.item[indexPath.row]
            cell.fruitLabel.text = collectionItem.name
            cell.fruitImg.image = UIImage(named: collectionItem.img)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let category = category {
            let collectionItem = category.item[indexPath.row]
            print("Terpilih \(collectionItem.name)")
            if let detailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailFruitView") as? DetailFruitView {
                detailView.imgName = collectionItem.img
                detailView.itemName = collectionItem.name
                self.window?.rootViewController?.present(detailView, animated: true, completion: nil)
            }
        }
    }
}
