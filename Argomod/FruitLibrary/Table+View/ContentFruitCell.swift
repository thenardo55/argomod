//
//  ContentFruitCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 25/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class ContentFruitCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var ContentCollectionView: UICollectionView!
    @IBOutlet weak var contentPageCtrl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentPageCtrl.numberOfPages = 3
        contentPageCtrl.currentPage = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentItem", for: indexPath) as! ContentCell
        
        // untuk konfigurasi tampilan
        
        cell.contentView.layer.cornerRadius = 10.0
        cell.contentView.layer.borderWidth = 0.1
        cell.contentView.layer.borderColor = UIColor.black.cgColor
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width:0, height: 2.0)
        cell.layer.shadowRadius = 1.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        //let ex = exerciseName[indexPath.row]
        // untuk populate data
        
        cell.contentImg.image = UIImage(named: "contentPlaceholder")
        cell.contentLabel.text = "New Veggies for your salad"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 20.0), height: 300)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/contentView.frame.width)
        contentPageCtrl.currentPage = Int(pageIndex)
    }
    
}
