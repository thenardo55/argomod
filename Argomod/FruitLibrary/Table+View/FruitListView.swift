//
//  FruitListView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 06/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class FruitListView: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    @IBOutlet weak var fruitTable: UITableView!
    
    //var kategori = ["Fruit", "Veggie", "Spice"]
    var contents:[Content] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fruitTable.dataSource = self
        fruitTable.delegate = self
        
        // tambahkan LibraryHeaderView utk custom header table
        let headerNib = UINib(nibName: "LibraryHeaderView", bundle: nil)
        fruitTable.register(headerNib, forHeaderFooterViewReuseIdentifier: "libraryHeaderView")
        
        //contents = generateContentScrollView()
    }
    
    func generateContentScrollView() -> [Content] {
        let content1:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content1.contentImg.image = UIImage(named: "contentPlaceholder")
        content1.contentLabel.text = "New Fruits for your breakfast"
        
        let content2:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content2.contentImg.image = UIImage(named: "contentPlaceholder")
        content2.contentLabel.text = "New Veggie for your Salad"
        
        let content3:Content = Bundle.main.loadNibNamed("Content", owner: self, options: nil)?.first as! Content
        content3.contentImg.image = UIImage(named: "contentPlaceholder")
        content3.contentLabel.text = "You will float too..."
        
        return [content1, content2, content3]
    }
    
    // MARK: - Table
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ItemCatalog.sharedInstance.category.count + 1
    }

//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return ItemCatalog.sharedInstance.category[section].name
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section > 0 {
            return 30
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section > 0 {
            let header = self.fruitTable.dequeueReusableHeaderFooterView(withIdentifier: "libraryHeaderView") as! LibraryHeaderView
            header.kategoriLabel.text = ItemCatalog.sharedInstance.category[section-1].name
            header.seeAllBtn.setTitle("See all", for: .normal)
            header.seeAllBtn.selectedKategori = ItemCatalog.sharedInstance.category[section-1].name
            header.seeAllBtn.selectedKategoriIndex = section
            header.seeAllBtn.addTarget(self, action: #selector(seeEverything(_:)), for: .touchUpInside)
            return header
        } else {
            
            return nil
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section > 0 {
            return 160
        } else {
            return 320
        }
    }
    
    private func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainFruitItem") as! MainFruitCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MainFruitItem"
        
        if indexPath.section > 0 {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                as? MainFruitCell else {
                    fatalError("The dequened cell is not an instance of MainFruitCell")
            }
            
            cell.category = ItemCatalog.sharedInstance.category[indexPath.section - 1]
            
            return cell
            
        } else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "contentFruitCell", for: indexPath) as? ContentFruitCell else {
                fatalError("The dequened cell is not an instance of ContentFruitCell")
            }
            
            return cell
            
        }
    }
    
    // MARK: - Action Button
    
    @objc func seeEverything(_ sender: UIButton) {
        let selectedKategori = (sender as! SeeAllButton).selectedKategori
        let selectedKategoriIndex = (sender as! SeeAllButton).selectedKategoriIndex
        let seeAllList = self.storyboard?.instantiateViewController(withIdentifier: "seeAllListView") as! SeeAllListView
        seeAllList.label = "\(selectedKategori)"
        seeAllList.index = selectedKategoriIndex
        self.navigationController?.pushViewController(seeAllList, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
