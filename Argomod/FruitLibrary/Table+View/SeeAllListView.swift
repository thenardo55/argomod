//
//  SeeAllListView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 12/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class SeeAllListView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var seeAllCollectionView: UICollectionView!
    
    var label: String = ""
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = label
        
        seeAllCollectionView.delegate = self
        seeAllCollectionView.dataSource = self
    }
    
    // MARK: - Collection Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ItemCatalog.sharedInstance.category[index-1].item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seeAllCell", for: indexPath) as! SeeAllCell
        
        // untuk konfigurasi tampilan
        
        cell.contentView.layer.cornerRadius = 20.0
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = UIColor.black.cgColor
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width:0, height: 2.0)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        // untuk populate data
        
        let collectionItem = ItemCatalog.sharedInstance.category[index-1].item[indexPath.row]
        cell.seeAllFruitLabel.text = collectionItem.name
        cell.seeAllFruitImg.image = UIImage(named: collectionItem.img)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let collectionItem = ItemCatalog.sharedInstance.category[index-1].item[indexPath.row]
        print("Terpilih \(collectionItem.name)")
        if let detailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailFruitView") as? DetailFruitView {
            detailView.imgName = collectionItem.img
            detailView.itemName = collectionItem.name
            self.present(detailView, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: (UIScreen.main.bounds.width - 20.0), height: 140)
        return CGSize(width: 160, height: 160)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
