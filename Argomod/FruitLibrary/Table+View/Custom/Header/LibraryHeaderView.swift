//
//  LibraryHeaderView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class LibraryHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var kategoriLabel: UILabel!
    @IBOutlet weak var seeAllBtn: SeeAllButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
