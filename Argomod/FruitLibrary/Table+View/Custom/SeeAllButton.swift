//
//  SeeAllButton.swift
//  Argomod
//
//  Created by Thenardo Ardo on 12/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Foundation

class SeeAllButton: UIButton {
    
    var selectedKategori: String = ""
    var selectedKategoriIndex: Int = 0
    
    convenience init(selectedKategori: String, indexKategori: Int) {
        self.init()
        self.selectedKategori = selectedKategori
        self.selectedKategoriIndex = indexKategori
    }
}
