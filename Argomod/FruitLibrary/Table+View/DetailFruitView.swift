//
//  DetailFruitView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 16/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class DetailFruitView: UIViewController {

    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    var itemName: String = ""
    var imgName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.itemLabel.text = itemName
        self.itemImg.image = UIImage(named: imgName)
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
