//
//  ViewController.swift
//  Argomod
//
//  Created by Thenardo Ardo on 06/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Lottie
import UserNotifications

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var pressedLabel: UILabel!
    @IBOutlet weak var selectedImg: UIImageView!
    var imagePicker = UIImagePickerController()
    let userSetting = UserDefaults.standard
    var pressed: Int = 0
    
    @IBOutlet weak var animationOutlet: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }
        
        pressed = userSetting.integer(forKey: "pressedTimes")
        pressedLabel.text = "Pressed: \(pressed)"
        
        let anim = Animation.named("simpleLoader")
        self.animationOutlet.animation = anim
        self.animationOutlet.contentMode = .scaleAspectFill
        self.animationOutlet.loopMode = .loop
        self.animationOutlet.play()
    }

    @IBAction func openImageGallery(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else { return }
        selectedImg.image = image
    }
    
    @IBAction func sendNotificationAction(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "FreshThumb"
        content.subtitle = "Did you know?"
        content.body = "Buah Pisang berguna untuk menambah energi yang membuatmu bersemangat"
        content.sound = UNNotificationSound.default
        
        // image in notification
        let imgUrl = Bundle.main.url(forResource: "minion", withExtension: "jpg")
        let attachment = try! UNNotificationAttachment(identifier: "image", url: imgUrl!, options: .none)
        content.attachments = [attachment]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let req = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)
    }
    
    @IBAction func increment(_ sender: UIButton) {
        self.pressed = pressed + 1
        userSetting.set(pressed, forKey: "pressedTimes")
        pressedLabel.text = "Pressed: \(pressed)"
    }
    
}

