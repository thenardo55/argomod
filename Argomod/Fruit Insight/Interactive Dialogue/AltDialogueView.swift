//
//  AltDialogueView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 28/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Lottie

class AltDialogueView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var dialogueTableView: UITableView!
    @IBOutlet weak var optionsView: UIView!
    
    @IBOutlet weak var firstSelectionBtn: UIButton!
    @IBOutlet weak var secondSelectionBtn: UIButton!
    @IBOutlet weak var thirdSelectionBtn: UIButton!
    
    @IBOutlet weak var firstSelectionWidth: NSLayoutConstraint!
    @IBOutlet weak var secondSelectionWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdSelectionWidth: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    
    let dateFormatter = DateFormatter()
    var msg: [Chat] = []
    
    // currChatState list: "001", "001-FRUIT", "002-FRUIT"
    var currChatState: String = "001"
    
    // Tipe message = 'text', 'img', 'sticker-img', 'anim'
    // *Tipe message 'anim' menggunakan string nama file json pada parameter 'message'
    
    // MARK: - Inisialisasi
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightConstant.constant = 230 // I = 90, II = 150, III = 230
        
        // Do any additional setup after loading the view.
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        // button tag utk mengenali pilihan dialog
        firstSelectionBtn.tag = 1
        secondSelectionBtn.tag = 2
        thirdSelectionBtn.tag = 3
        
        // inisial button
        firstSelectionBtn.setTitle("Tell me about fruits 🍎", for: .normal)
        secondSelectionBtn.setTitle("Tell me about vegetables 🥦", for: .normal)
        thirdSelectionBtn.setTitle("-Pilih untuk kebutuhan testing-", for: .normal)
        
        // Table
        dialogueTableView.delegate = self
        dialogueTableView.dataSource = self
        
        dialogueTableView.rowHeight = UITableView.automaticDimension
        dialogueTableView.estimatedRowHeight = 70
        
        // greet
        initialChatChoice()
        initOptionBtns()
        initialGreetFromChatbot()
    }
    
    func initOptionBtns() {
        
        // option 1
        firstSelectionBtn.layer.cornerRadius = 23
        firstSelectionBtn.layer.borderWidth = 1.5
        firstSelectionBtn.layer.borderColor = #colorLiteral(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1)
        // == calculate the width
        firstSelectionWidth.constant = firstSelectionBtn.intrinsicContentSize.width + 40
        
        // option 2
        secondSelectionBtn.layer.cornerRadius = 23
        secondSelectionBtn.layer.borderWidth = 1.5
        secondSelectionBtn.layer.borderColor = #colorLiteral(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1)
        // == calculate the width
        secondSelectionWidth.constant = secondSelectionBtn.intrinsicContentSize.width + 40
        
        // option 3
        thirdSelectionBtn.layer.cornerRadius = 23
        thirdSelectionBtn.layer.borderWidth = 1.5
        thirdSelectionBtn.layer.borderColor = #colorLiteral(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1)
        // == calculate the width
        thirdSelectionWidth.constant = thirdSelectionBtn.intrinsicContentSize.width + 40
    }
    
    func initialChatChoice() {
        firstSelectionBtn.setTitle("Absolutely!", for: .normal)
        secondSelectionBtn.setTitle("Sure", for: .normal)
        thirdSelectionBtn.setTitle("Maybe next time", for: .normal)
    }
    
    func initialGreetFromChatbot() {
        // Matikan tombol Send sementara
        disableSendButton()
        
        // Siapkan animasi chat sebagai loading
        let dummyChat1 = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "simpleLoader".data(using: .utf8)!, msgType: "anim")
        msg.append(dummyChat1)
        dialogueTableView.reloadData()
        scrollToNewestMessage()
        
        // Output reply chat aktual setelah 1 detik
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.msg.removeLast()
            
            let fruitImg = UIImage(named: "tamtam-greet")
            
            let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Hello!".data(using: .utf8)!, msgType: "text")
            let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "sticker-img")
            let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Today I want to share\nmy secret for a healthier breakfast!".data(using: .utf8)!, msgType: "text")
            let newReply4 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Do you want to know about it?".data(using: .utf8)!, msgType: "text")
            self.msg.append(newReply1)
            self.msg.append(newReply2)
            self.msg.append(newReply3)
            self.msg.append(newReply4)
            
            self.dialogueTableView.reloadData()
            self.enableSendButton()
            self.scrollToNewestMessage()
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // cell default yg di return jika tidak ada tipe message yg spesifik
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueTextCell", for: indexPath)
            as? ChatTextCell else {
                fatalError("The dequened cell is not an instance of DialogueCell")
        }
        
        let msgContent = msg[indexPath.row]
        
        if msgContent.isSender {
            
            if msgContent.msgType == "text" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "selfDialogueTextCell", for: indexPath)
                    as? SelfChatTextCell else {
                        fatalError("The dequened cell is not an instance of DialogueTextCell")
                }
                
                // tampilan msg
                cell.msgLabel.layer.cornerRadius = 22
                //cell.msgLabel.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgLabel.clipsToBounds = true
                cell.msgLabel.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgLabel.text = String(data: msgContent.message, encoding: .utf8)
                cell.msgLabel.textAlignment = .right
                cell.msgLabel.textColor = .white
                cell.msgLabel.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "img" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "selfDialogueImgCell", for: indexPath)
                    as? SelfChatImageCell else {
                        fatalError("The dequened cell is not an instance of SelfDialogueImgCell")
                }
                
                // tampilan msg
                cell.msgImg.layer.borderWidth = 4.0
                cell.msgImg.layer.borderColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                cell.msgImg.layer.cornerRadius = 22
                //cell.msgImg.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgImg.clipsToBounds = true
                cell.msgImg.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgImg.image = UIImage(data: msgContent.message)
                cell.msgImg.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                
                return cell
            }
            
        } else {
            
            if msgContent.msgType == "text" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueTextCell", for: indexPath)
                    as? ChatTextCell else {
                        fatalError("The dequened cell is not an instance of DialogueTextCell")
                }
                
                // tampilan msg
                cell.msgLabel.layer.cornerRadius = 22
                //cell.msgLabel.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgLabel.clipsToBounds = true
                cell.msgLabel.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgLabel.text = String(data: msgContent.message, encoding: .utf8)
                cell.msgLabel.textAlignment = .left
                cell.msgLabel.textColor = .black
                cell.msgLabel.backgroundColor = #colorLiteral(red: 1, green: 0.8196078431, blue: 0, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "sticker-img" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueImgCell", for: indexPath)
                    as? ChatImageCell else {
                        fatalError("The dequened cell is not an instance of DialogueImgCell")
                }
                
                // tampilan msg
                cell.msgImg.layer.borderWidth = 4.0
                cell.msgImg.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                cell.msgImg.layer.cornerRadius = 22
                //cell.msgImg.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgImg.clipsToBounds = true
                cell.msgImg.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgImg.image = UIImage(data: msgContent.message)
                cell.msgImg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                
                return cell
                
            } else if msgContent.msgType == "img" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueImgCell", for: indexPath)
                    as? ChatImageCell else {
                        fatalError("The dequened cell is not an instance of DialogueImgCell")
                }
                
                // tampilan msg
                cell.msgImg.layer.borderWidth = 4.0
                cell.msgImg.layer.borderColor = #colorLiteral(red: 1, green: 0.8196078431, blue: 0, alpha: 1)
                cell.msgImg.layer.cornerRadius = 22
                //cell.msgImg.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgImg.clipsToBounds = true
                cell.msgImg.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgImg.image = UIImage(data: msgContent.message)
                cell.msgImg.contentMode = .scaleAspectFill
                cell.msgImg.backgroundColor = #colorLiteral(red: 1, green: 0.8196078431, blue: 0, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "anim" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
                    as? ChatLoadingCell else {
                        fatalError("The dequened cell is not an instance of LoadingCell")
                }
                
                // tampilan msg
                cell.loadingContainer.layer.cornerRadius = 12
                //cell.loadingContainer.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.loadingContainer.clipsToBounds = true
                cell.loadingContainer.layer.masksToBounds = true
                cell.loadingContainer.backgroundColor = UIColor(red: 0.8093737364, green: 0.8666228652, blue: 0.7942800522, alpha: 0.4)
                
                // konten animasi
                cell.loadingOutlet.animation = Animation.named("simpleLoader")
                cell.loadingOutlet.contentMode = .scaleAspectFill
                cell.loadingOutlet.loopMode = .loop
                cell.loadingOutlet.play()
                
                return cell
                
            }
            
        }
        
        return cell
    }
    
    // MARK: - Dismiss View
    
    @IBAction func dismissChatRoom(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Function Pengaturan Jumlah Chat Options
    
    func toggleOptions(num: Int) {
        
        if num == 1 {
            
            UIView.animate(
                withDuration: 0.3,
                delay: 0.1,
                options: .curveEaseInOut,
                animations: { () -> Void in
                
                    self.heightConstant.constant = 90
                    self.view.layoutIfNeeded()
                    
                    self.secondSelectionBtn.isHidden = true
                    self.thirdSelectionBtn.isHidden = true
            })
            
        } else if num == 2 {
            
            UIView.animate(
                withDuration: 0.3,
                delay: 0.1,
                options: .curveEaseInOut,
                animations: { () -> Void in
                
                    self.heightConstant.constant = 150
                    self.view.layoutIfNeeded()
                    
                    self.secondSelectionBtn.isHidden = false
                    self.thirdSelectionBtn.isHidden = true
            })
            
        } else if num == 3 {
            
            UIView.animate(
                withDuration: 0.3,
                delay: 0.1,
                options: .curveEaseInOut,
                animations: { () -> Void in
                
                    self.heightConstant.constant = 230
                    self.view.layoutIfNeeded()
                    
                    self.secondSelectionBtn.isHidden = false
                    self.thirdSelectionBtn.isHidden = false
            })
            
        } else {
            
            // none...
        }
        
    }
    
    // MARK: - Function Pilihan Chat
    
    @IBAction func firstChatChoice(_ sender: UIButton) {
        
        if currChatState == "001" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Absolutely!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "002-0" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "That is good to know".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "002-1" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Got it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "002-2" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Love it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "002-3" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Watermelon".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "003" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Thank you".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "004" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Yeah!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        } else if currChatState == "005" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Alright".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 1)
            
        }
        
    }
    
    @IBAction func secondChatChoice(_ sender: UIButton) {
        
        if currChatState == "001" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Sure!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "002-0" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "That is good to know".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "002-1" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Got it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "002-2" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Love it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "002-3" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Mango".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "003" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Thank you".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "004" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Yes!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        } else if currChatState == "005" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Okay".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 2)
            
        }
        
    }
    
    @IBAction func thirdChatChoice(_ sender: UIButton) {
        
        if currChatState == "001" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Maybe next time".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "002-0" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "That is good to know".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "002-1" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Got it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "002-2" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Love it!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "002-3" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "I love them all!".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "003" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Thank you".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "004" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Not really. Sorry.".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        } else if currChatState == "005" {
            let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Maybe next time".data(using: .utf8)!, msgType: "text")
            self.msg.append(newChat)
            self.dialogueTableView.reloadData()
            self.scrollToNewestMessage()
            
            getReplyFromChatbot(choice: 3)
            
        }
        
    }
    
    // MARK: - Function Efek Reaksi Chat
    
    func enableSendButton() {
        firstSelectionBtn.isEnabled = true
        secondSelectionBtn.isEnabled = true
        thirdSelectionBtn.isEnabled = true
        
        if #available(iOS 13.0, *) {
            firstSelectionBtn.backgroundColor = UIColor.systemBackground
            secondSelectionBtn.backgroundColor = UIColor.systemBackground
            thirdSelectionBtn.backgroundColor = UIColor.systemBackground
        } else {
            firstSelectionBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            secondSelectionBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            thirdSelectionBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        firstSelectionBtn.setTitleColor(UIColor.init(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1), for: .normal)
        secondSelectionBtn.setTitleColor(UIColor.init(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1), for: .normal)
        thirdSelectionBtn.setTitleColor(UIColor.init(red: 0.6213095188, green: 0.7284798026, blue: 0.2289550006, alpha: 1), for: .normal)
    }
    
    func disableSendButton() {
        firstSelectionBtn.isEnabled = false
        secondSelectionBtn.isEnabled = false
        thirdSelectionBtn.isEnabled = false
        
        firstSelectionBtn.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        secondSelectionBtn.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        thirdSelectionBtn.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        firstSelectionBtn.setTitleColor(UIColor.init(red: 0.8, green: 0.8, blue: 0.8, alpha: 1), for: .normal)
        secondSelectionBtn.setTitleColor(UIColor.init(red: 0.8, green: 0.8, blue: 0.8, alpha: 1), for: .normal)
        thirdSelectionBtn.setTitleColor(UIColor.init(red: 0.8, green: 0.8, blue: 0.8, alpha: 1), for: .normal)
    }
    
    func calculateButtonWidth() {
        firstSelectionWidth.constant = firstSelectionBtn.intrinsicContentSize.width + 40
        secondSelectionWidth.constant = secondSelectionBtn.intrinsicContentSize.width + 40
        thirdSelectionWidth.constant = thirdSelectionBtn.intrinsicContentSize.width + 40
    }
    
    func scrollToNewestMessage() {
        let getNumOfSections = dialogueTableView.numberOfSections
        let getNumOfRows = dialogueTableView.numberOfRows(inSection: getNumOfSections-1)
        
        let indexPath = IndexPath(row: getNumOfRows-1 , section: getNumOfSections-1)
        dialogueTableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: true)
    }
    
    func triggerChatAfterEffect(isEnd: Bool) {
        dialogueTableView.reloadData()
        if !isEnd {
            enableSendButton()
        }
        scrollToNewestMessage()
    }
    
    // MARK: - Function Reaksi Chat TamTam
    
    func getReplyFromChatbot(choice: Int) {
        
        // Matikan tombol Send sementara
        disableSendButton()
        
        // Siapkan animasi chat sebagai loading
        let dummyChat = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "simpleLoader".data(using: .utf8)!, msgType: "anim")
        msg.append(dummyChat)
        dialogueTableView.reloadData()
        scrollToNewestMessage()
        
        // Output reply chat aktual setelah 1 detik
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.msg.removeLast()
            
            // MARK: - IF CHOOSE OPTION 1
            
            if choice == 1 {
                
                if self.currChatState == "001" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Yay! 😊".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Do you know that watermelon and mango is really good for breakfast?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    
                    self.currChatState = "002-0"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("That is good to know", for: .normal)
                    self.secondSelectionBtn.setTitle("That is good to know", for: .normal)
                    self.thirdSelectionBtn.setTitle("That is good to know", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-0" {
                    
                    let fruitImg = UIImage(named: "gambar-watermelon")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Watermelon is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-1"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Got it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Got it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Got it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-1" {
                    
                    let fruitImg = UIImage(named: "gambar-mango")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I'm sure you love this fruit!".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Mango is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3]
                    
                    self.currChatState = "002-2"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Love it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Love it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Love it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-2" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "So, which one of these is".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "your favorite?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-3"
                    
                    self.toggleOptions(num: 3)
                    
                    self.firstSelectionBtn.setTitle("Watermelon", for: .normal)
                    self.secondSelectionBtn.setTitle("Mango", for: .normal)
                    self.thirdSelectionBtn.setTitle("I love them all!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-3" {
                    
                    let fruitImg = UIImage(named: "tamtam-happy")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "sticker-img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Great choice!".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I hope you have a".data(using: .utf8)!, msgType: "text")
                    let newReply4 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "great breakfast today!".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3, newReply4]
                    
                    self.currChatState = "003"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.secondSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Thank you!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "003" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Do you enjoy this conversation?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.currChatState = "004"
                    
                    self.toggleOptions(num: 2)
                    
                    self.firstSelectionBtn.setTitle("Yeah!", for: .normal)
                    self.secondSelectionBtn.setTitle("Yes!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Not really. Sorry", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "004" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Awesome. I love talking\nwith you too".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "If you want to have more\ninteresting chat with me don't forget to subscribe Freshthumb ☺️".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    
                    self.currChatState = "005"
                    
                    self.toggleOptions(num: 3)
                    
                    self.firstSelectionBtn.setTitle("Alright", for: .normal)
                    self.secondSelectionBtn.setTitle("Okay", for: .normal)
                    self.thirdSelectionBtn.setTitle("Maybe next time", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "005" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Alright then 😉".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.triggerChatAfterEffect(isEnd: true)
                    
                }
                
            // MARK: - IF CHOOSE OPTION 2
                
            } else if choice == 2 {
                
                if self.currChatState == "001" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Yay! 😊".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Do you know that watermelon and mango is really good for breakfast?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    
                    self.currChatState = "002-0"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("That is good to know", for: .normal)
                    self.secondSelectionBtn.setTitle("That is good to know", for: .normal)
                    self.thirdSelectionBtn.setTitle("That is good to know", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-0" {
                    
                    let fruitImg = UIImage(named: "gambar-watermelon")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Watermelon is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-1"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Got it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Got it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Got it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-1" {
                    
                    let fruitImg = UIImage(named: "gambar-mango")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I'm sure you love this fruit!".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Mango is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3]
                    
                    self.currChatState = "002-2"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Love it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Love it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Love it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-2" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "So, which one of these is".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "your favorite?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-3"
                    
                    self.toggleOptions(num: 3)
                    
                    self.firstSelectionBtn.setTitle("Watermelon", for: .normal)
                    self.secondSelectionBtn.setTitle("Mango", for: .normal)
                    self.thirdSelectionBtn.setTitle("I love them all!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-3" {
                    
                    let fruitImg = UIImage(named: "tamtam-happy")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "sticker-img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Great choice!".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I hope you have a".data(using: .utf8)!, msgType: "text")
                    let newReply4 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "great breakfast today!".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3, newReply4]
                    
                    self.currChatState = "003"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.secondSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Thank you!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "003" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Do you enjoy this conversation?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.currChatState = "004"
                    
                    self.toggleOptions(num: 2)
                    
                    self.firstSelectionBtn.setTitle("Yeah!", for: .normal)
                    self.secondSelectionBtn.setTitle("Yes!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Not really. Sorry", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "004" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Awesome. I love talking\nwith you too".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "If you want to have more\ninteresting chat with me don't forget to subscribe Freshthumb ☺️".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    
                    self.currChatState = "005"
                    
                    self.toggleOptions(num: 3)
                    
                    self.firstSelectionBtn.setTitle("Alright", for: .normal)
                    self.secondSelectionBtn.setTitle("Okay", for: .normal)
                    self.thirdSelectionBtn.setTitle("Maybe next time", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "005" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Alright then 😉".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: true)
                    
                }
            
            // MARK: - IF CHOOSE OPTION 3
                
            } else if choice == 3 {
                
                if self.currChatState == "001" {
                    
                    let fruitImg = UIImage(named: "tamtam-sad")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "sticker-img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Okay. Maybe next time 🙁".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: true)
                    
                } else if self.currChatState == "002-0" {
                    
                    let fruitImg = UIImage(named: "gambar-watermelon")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Watermelon is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-1"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Got it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Got it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Got it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-1" {
                    
                    let fruitImg = UIImage(named: "gambar-mango")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I'm sure you love this fruit!".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Mango is ...(short explaination)".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3]
                    
                    self.currChatState = "002-2"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Love it!", for: .normal)
                    self.secondSelectionBtn.setTitle("Love it!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Love it!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-2" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "So, which one of these is".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "your favorite?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2]
                    
                    self.currChatState = "002-3"
                    
                    self.toggleOptions(num: 3)
                    
                    self.firstSelectionBtn.setTitle("Watermelon", for: .normal)
                    self.secondSelectionBtn.setTitle("Mango", for: .normal)
                    self.thirdSelectionBtn.setTitle("I love them all!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "002-3" {
                    
                    let fruitImg = UIImage(named: "tamtam-impressed")
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "sticker-img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Wonderful! I love them all too.".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "I hope you have a".data(using: .utf8)!, msgType: "text")
                    let newReply4 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "great breakfast today!".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply1, newReply2, newReply3, newReply4]
                    
                    self.currChatState = "003"
                    
                    self.toggleOptions(num: 1)
                    
                    self.firstSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.secondSelectionBtn.setTitle("Thank you!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Thank you!", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "003" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Do you enjoy this conversation?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.currChatState = "004"
                    
                    self.toggleOptions(num: 2)
                    
                    self.firstSelectionBtn.setTitle("Yeah!", for: .normal)
                    self.secondSelectionBtn.setTitle("Yes!", for: .normal)
                    self.thirdSelectionBtn.setTitle("Not really. Sorry", for: .normal)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: false)
                    
                } else if self.currChatState == "004" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Well I hope I can bring you".data(using: .utf8)!, msgType: "text")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "a better topic next time.".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "See you.".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    self.msg.append(newReply2)
                    self.msg.append(newReply3)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: true)
                    
                } else if self.currChatState == "005" {
                    
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Alright then 😉".data(using: .utf8)!, msgType: "text")
                    
                    self.msg.append(newReply1)
                    
                    self.calculateButtonWidth()
                    
                    self.triggerChatAfterEffect(isEnd: true)
                    
                }
                
            }

        }
    }
    
}
