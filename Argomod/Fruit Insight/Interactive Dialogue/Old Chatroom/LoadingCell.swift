//
//  LoadingCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 03/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Lottie

class LoadingCell: UITableViewCell {

    @IBOutlet weak var loadingContainer: UIView!
    @IBOutlet weak var loadingOutlet: AnimationView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
