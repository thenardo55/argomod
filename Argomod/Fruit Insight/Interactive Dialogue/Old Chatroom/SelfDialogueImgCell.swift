//
//  SelfDialogueImgCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 02/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class SelfDialogueImgCell: UITableViewCell {

    @IBOutlet weak var msgImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
