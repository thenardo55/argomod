//
//  DialogueCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 27/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class DialogueTextCell: UITableViewCell {

    @IBOutlet weak var msgLabel: CustomDialogueLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
