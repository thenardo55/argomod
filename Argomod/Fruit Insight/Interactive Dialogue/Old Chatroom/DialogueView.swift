//
//  DialogueView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 27/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Lottie

class DialogueView: UIViewController, UITableViewDataSource, UITableViewDelegate, SSRadioButtonControllerDelegate {
   
    @IBOutlet weak var dialogueTable: UITableView!
    @IBOutlet weak var dialogueSelectionView: UIView!
    @IBOutlet weak var chatBtn: UIButton!
    
    var chatRadioButtonController: SSRadioButtonsController?
    
    @IBOutlet weak var chatOption01: SSRadioButton!
    @IBOutlet weak var chatOption02: SSRadioButton!
    @IBOutlet weak var chatOption03: SSRadioButton!
    
    let dateFormatter = DateFormatter()
    var msg: [Chat] = []
    
    // currChatState list: "001", "001-FRUIT", "002-FRUIT"
    var currChatState: String = "001"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        //generateDummyMessage()
        
        // button tag untuk mengenali pilihan dialog
        chatOption01.tag = 1
        chatOption02.tag = 2
        chatOption03.tag = 3
        
        // Table
        dialogueTable.delegate = self
        dialogueTable.dataSource = self
        
        dialogueTable.rowHeight = UITableView.automaticDimension
        dialogueTable.estimatedRowHeight = 70
        
        // Chat Selection View
        dialogueSelectionView.layer.cornerRadius = 12
        dialogueSelectionView.clipsToBounds = true
        dialogueSelectionView.layer.masksToBounds = true
        dialogueSelectionView.layer.borderWidth = 2.0
        dialogueSelectionView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        
        // Radio Button
        chatRadioButtonController = SSRadioButtonsController(buttons: chatOption01, chatOption02, chatOption03)
        chatRadioButtonController?.delegate = self
        chatRadioButtonController?.shouldLetDeSelect = true
        
        // Send Button
        chatBtn.isEnabled = false
        chatBtn.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        // Misc
        chatOption03.setTitle("-Pilih untuk kebutuhan testing-", for: .normal)
        
        // greet
        initialGreetFromChatbot()
    }
    
    func generateDummyMessage() {
        // Tipe message = 'text', 'img', 'anim'
        // *Tipe message 'anim' menggunakan string nama file json pada parameter 'message'
        
        let chat1 = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:00")!, sender: "Kal", isSender: false, message: "Hello there.".data(using: .utf8)!, msgType: "text")
        let chat2 = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:10")!, sender: "Kal", isSender: false, message: "Is there anything that I can help?".data(using: .utf8)!, msgType: "text")
        
        msg += [chat1, chat2]
    }
    
    func initialGreetFromChatbot() {
        // Matikan tombol Send sementara
        disableSendButton()
        
        // Siapkan animasi chat sebagai loading
        let dummyChat1 = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "simpleLoader".data(using: .utf8)!, msgType: "anim")
        msg.append(dummyChat1)
        dialogueTable.reloadData()
        scrollToNewestMessage()
        
        // Output reply chat aktual setelah 1 detik
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.msg.removeLast()
            
            let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Hello there".data(using: .utf8)!, msgType: "text")
            let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Is there anything that I can help?".data(using: .utf8)!, msgType: "text")
            self.msg.append(newReply1)
            self.msg.append(newReply2)
            
            self.dialogueTable.reloadData()
            self.enableSendButton()
            self.scrollToNewestMessage()
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // cell default yg di return jika tidak ada tipe message yg spesifik
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueTextCell", for: indexPath)
            as? DialogueTextCell else {
                fatalError("The dequened cell is not an instance of DialogueCell")
        }
        
        let msgContent = msg[indexPath.row]
        
        if msgContent.isSender {
            
            if msgContent.msgType == "text" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "selfDialogueTextCell", for: indexPath)
                    as? SelfDialogueTextCell else {
                        fatalError("The dequened cell is not an instance of DialogueTextCell")
                }
                
                // tampilan msg
                cell.msgLabel.layer.cornerRadius = 12
                cell.msgLabel.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgLabel.clipsToBounds = true
                cell.msgLabel.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgLabel.text = String(data: msgContent.message, encoding: .utf8)
                cell.msgLabel.textAlignment = .right
                cell.msgLabel.textColor = .white
                cell.msgLabel.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "img" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "selfDialogueImgCell", for: indexPath)
                    as? SelfDialogueImgCell else {
                        fatalError("The dequened cell is not an instance of SelfDialogueImgCell")
                }
                
                // tampilan msg
                cell.msgImg.layer.borderWidth = 4.0
                cell.msgImg.layer.borderColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                cell.msgImg.layer.cornerRadius = 12
                cell.msgImg.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgImg.clipsToBounds = true
                cell.msgImg.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgImg.image = UIImage(data: msgContent.message)
                cell.msgImg.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.6862745098, blue: 0.1764705882, alpha: 1)
                
                return cell
            }
            
        } else {
            
            if msgContent.msgType == "text" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueTextCell", for: indexPath)
                    as? DialogueTextCell else {
                        fatalError("The dequened cell is not an instance of DialogueTextCell")
                }
                
                // tampilan msg
                cell.msgLabel.layer.cornerRadius = 12
                cell.msgLabel.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgLabel.clipsToBounds = true
                cell.msgLabel.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgLabel.text = String(data: msgContent.message, encoding: .utf8)
                cell.msgLabel.textAlignment = .left
                cell.msgLabel.textColor = .black
                cell.msgLabel.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.8392156863, blue: 0.7490196078, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "img" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "dialogueImgCell", for: indexPath)
                    as? DialogueImgCell else {
                        fatalError("The dequened cell is not an instance of DialogueImgCell")
                }
                
                // tampilan msg
                cell.msgImg.layer.borderWidth = 4.0
                cell.msgImg.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.8392156863, blue: 0.7490196078, alpha: 1)
                cell.msgImg.layer.cornerRadius = 12
                cell.msgImg.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.msgImg.clipsToBounds = true
                cell.msgImg.layer.masksToBounds = true
                
                // konten dan konfigurasi msg
                cell.msgImg.image = UIImage(data: msgContent.message)
                cell.msgImg.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.8392156863, blue: 0.7490196078, alpha: 1)
                
                return cell
                
            } else if msgContent.msgType == "anim" {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
                    as? LoadingCell else {
                        fatalError("The dequened cell is not an instance of LoadingCell")
                }
                
                // tampilan msg
                cell.loadingContainer.layer.cornerRadius = 12
                cell.loadingContainer.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                cell.loadingContainer.clipsToBounds = true
                cell.loadingContainer.layer.masksToBounds = true
                cell.loadingContainer.backgroundColor = UIColor(red: 0.8093737364, green: 0.8666228652, blue: 0.7942800522, alpha: 0.4)
                
                // konten animasi
                cell.loadingOutlet.animation = Animation.named("simpleLoader")
                cell.loadingOutlet.contentMode = .scaleAspectFill
                cell.loadingOutlet.loopMode = .loop
                cell.loadingOutlet.play()
                
                return cell
                
            }
            
        }
        
        return cell
    }
    
    // MARK: - Action Functions
    
    // MARK: - Delegate SS Radio Buttons
    
    func didSelectButton(selectedButton: UIButton?) {
        print(selectedButton?.titleLabel?.text! ?? "")
        print(selectedButton!.tag)
        if selectedButton != nil {
            enableSendButton()
        } else {
            disableSendButton()
        }
    }
    
    // MARK: - Dismiss View
    
    @IBAction func dismissDialogue(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Chat Functions
    
    @IBAction func initiateChat(_ sender: UIButton) {
        
        let selectedRadioBtn = chatRadioButtonController?.selectedButton()
        
        if currChatState == "001" {
            
            if selectedRadioBtn?.tag == 1 {
                
                let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "I want to know about fruit.".data(using: .utf8)!, msgType: "text")
                self.msg.append(newChat)
                self.dialogueTable.reloadData()
                self.scrollToNewestMessage()
                
                getReplyFromChatbot(choice: 1)
                
            } else if selectedRadioBtn?.tag == 2 {
                
                let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Can you tell me about one of vegetables?".data(using: .utf8)!, msgType: "text")
                self.msg.append(newChat)
                self.dialogueTable.reloadData()
                self.scrollToNewestMessage()
                
                getReplyFromChatbot(choice: 2)
                
            } else if selectedRadioBtn?.tag == 3 {
                
                let dialogueChoiceAlert = UIAlertController(title: nil, message: "Select of your choice", preferredStyle: .actionSheet)
                
                let choice1 = UIAlertAction(title: "Why?", style: .default) { action -> Void in
                    let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Why".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newChat)
                    self.dialogueTable.reloadData()
                    self.scrollToNewestMessage()
                }
                
                let choice2 = UIAlertAction(title: "I'm not sure", style: .default) { action -> Void in
                    let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "I'm not sure".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newChat)
                    self.dialogueTable.reloadData()
                    self.scrollToNewestMessage()
                }
                
                let choice3 = UIAlertAction(title: "GET REPLY", style: .default) { action -> Void in
                    
                    self.getReplyFromChatbot(choice: 3)
                }
                
                let choice4 = UIAlertAction(title: "COBA SEND IMG", style: .default) { action -> Void in
                    let newImg = UIImage(named: "meme1")
                    let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: (newImg?.pngData())!, msgType: "img")
                    self.msg.append(newChat)
                    self.dialogueTable.reloadData()
                    self.scrollToNewestMessage()
                }
                
                let choice5 = UIAlertAction(title: "Batal", style: .cancel)
                
                dialogueChoiceAlert.addAction(choice1)
                dialogueChoiceAlert.addAction(choice2)
                dialogueChoiceAlert.addAction(choice3)
                dialogueChoiceAlert.addAction(choice4)
                dialogueChoiceAlert.addAction(choice5)
                
                self.present(dialogueChoiceAlert, animated: true, completion: nil)
            }
            
        } else if currChatState == "001-FRUIT" {
            
            if selectedRadioBtn?.tag == 1 {
                
                let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Can you tell me what's fruits in this month?".data(using: .utf8)!, msgType: "text")
                self.msg.append(newChat)
                self.dialogueTable.reloadData()
                self.scrollToNewestMessage()
                
                getReplyFromChatbot(choice: 1)
                
            } else if selectedRadioBtn?.tag == 2 {
                
                let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "Can you tell health benefits contained in one of fruits?".data(using: .utf8)!, msgType: "text")
                self.msg.append(newChat)
                self.dialogueTable.reloadData()
                self.scrollToNewestMessage()
                
                getReplyFromChatbot(choice: 2)
                
            } else if selectedRadioBtn?.tag == 3 {
                
                let newChat = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "You", isSender: true, message: "I want to change topic.".data(using: .utf8)!, msgType: "text")
                self.msg.append(newChat)
                self.dialogueTable.reloadData()
                self.scrollToNewestMessage()
                
                getReplyFromChatbot(choice: 3)
                
            }
            
        }
    }
    
    func enableSendButton() {
        chatBtn.isEnabled = true
        chatBtn.backgroundColor = UIColor(red: 142/255, green: 175/255, blue: 45/255, alpha: 1)
    }
    
    func disableSendButton() {
        chatBtn.isEnabled = false
        chatBtn.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    func scrollToNewestMessage() {
        let getNumOfSections = dialogueTable.numberOfSections
        let getNumOfRows = dialogueTable.numberOfRows(inSection: getNumOfSections-1)
        
        let indexPath = IndexPath(row: getNumOfRows-1 , section: getNumOfSections-1)
        dialogueTable.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: true)
    }
    
    func getReplyFromChatbot(choice: Int) {
        
        // Matikan tombol Send sementara
        disableSendButton()
        
        // Siapkan animasi chat sebagai loading
        let dummyChat = Chat(dateSent: dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "simpleLoader".data(using: .utf8)!, msgType: "anim")
        msg.append(dummyChat)
        dialogueTable.reloadData()
        scrollToNewestMessage()
        
        // Output reply chat aktual setelah 1 detik
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.msg.removeLast()
            
            if choice == 1 {
                
                if self.currChatState == "001" {
                    
                    let newReply = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "So, what's spesific detail do you want to know?".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newReply)
                    
                    self.currChatState = "001-FRUIT"
                    
                    self.chatOption01.setTitle("Fruits in this month", for: .normal)
                    self.chatOption02.setTitle("Tell me health benefits in one of fruits", for: .normal)
                    self.chatOption03.setTitle("I want to ask other topic", for: .normal)
                    
                } else if self.currChatState == "001-FRUIT" {
                    
                    let newReply = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "I'm sorry, I don't any requested information to share for you at this time.\n😕".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newReply)
                    
                }
                
            } else if choice == 2 {
                
                if self.currChatState == "001" {
                    
                    let newReply = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "I'm sorry, I don't any requested information to share for you at this time.\n😕".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newReply)
                    
                } else if self.currChatState == "001-FRUIT" {
                    
                    let fruitImg = UIImage(named: "applePlaceholder")
                    
                    let newReply0 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Here we go...".data(using: .utf8)!, msgType: "text")
                    let newReply1 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: (fruitImg?.pngData())!, msgType: "img")
                    let newReply2 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Apple is one of the fruit that able to fill your stomach, keep you from feeling hunger for several hours. It's also best for your salad ingridient.".data(using: .utf8)!, msgType: "text")
                    let newReply3 = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 16:20")!, sender: "Kal", isSender: false, message: "Is there anything that I can help?".data(using: .utf8)!, msgType: "text")
                    
                    self.msg += [newReply0, newReply1, newReply2, newReply3]
                    
                }
                
            } else if choice == 3 {
                
                if self.currChatState == "001" {
                    
                    let newReply = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Hello, do you need something?".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newReply)
                    
                } else if self.currChatState == "001-FRUIT" {
                    
                    let newReply = Chat(dateSent: self.dateFormatter.date(from: "2020/01/08 17:10")!, sender: "Kal", isSender: false, message: "Alright, then".data(using: .utf8)!, msgType: "text")
                    self.msg.append(newReply)
                    
                    self.currChatState = "001"
                    
                    self.chatOption01.setTitle("Tell me about fruit 🍎", for: .normal)
                    self.chatOption02.setTitle("Tell me about vegetable 🥦", for: .normal)
                    self.chatOption03.setTitle("-Pilih untuk kebutuhan testing-", for: .normal)
                }
                
            }
            
            self.dialogueTable.reloadData()
            self.enableSendButton()
            self.scrollToNewestMessage()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
