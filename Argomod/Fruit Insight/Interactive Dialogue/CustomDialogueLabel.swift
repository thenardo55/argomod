//
//  CustomDialogueLabel.swift
//  Argomod
//
//  Created by Thenardo Ardo on 01/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class CustomDialogueLabel: UILabel {
    let padding = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: padding))
    }
    
    override var intrinsicContentSize: CGSize {
        let contentSize = super.intrinsicContentSize
        let width = contentSize.width + padding.left + padding.right
        let height = contentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: height)
    }
}
