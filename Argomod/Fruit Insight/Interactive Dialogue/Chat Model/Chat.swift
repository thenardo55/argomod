//
//  Chat.swift
//  Argomod
//
//  Created by Thenardo Ardo on 27/09/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class Chat {
    var dateSent: Date
    var sender: String
    var isSender: Bool
    var message: Data
    var msgType: String
    
    init(dateSent: Date, sender: String, isSender: Bool, message: Data, msgType: String) {
        self.dateSent = dateSent
        self.sender = sender
        self.isSender = isSender
        self.message = message
        self.msgType = msgType
    }
}
