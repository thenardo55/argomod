//
//  Chatbot.swift
//  Argomod
//
//  Created by Thenardo Ardo on 08/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class Chatbot {
    var id: String
    var response: String
    var tree: String
    
    init(id: String, response: String, tree: String) {
        self.id = id
        self.response = response
        self.tree = tree
    }
}
