//
//  AltArticleView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import AppstoreTransition

class AltArticleView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var articleCollectionList: UICollectionView!
    
    private var transition: CardTransition?
    
    var article: [AltArticle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        articleCollectionList.delegate = self
        articleCollectionList.dataSource = self
        
        generateArticleList()
    }
    
    func generateArticleList() {
        let ar1 = AltArticle(articleId: 0, articleTitle: "3", articleType: "chat", articleImageName: "banner-breakfast")
        let ar2 = AltArticle(articleId: 1, articleTitle: "1", articleType: "free", articleImageName: "banner-broccoli")
        let ar3 = AltArticle(articleId: 2, articleTitle: "2", articleType: "free", articleImageName: "banner-ginger")
        let ar4 = AltArticle(articleId: 3, articleTitle: "3", articleType: "free", articleImageName: "banner-citrus")
        
        article += [ar1, ar2, ar3, ar4]
    }
    
    // MARK: - Collection Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return article.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "altArticleCell", for: indexPath) as! AltArticleCell
        
        let articleContent = article[indexPath.row]
        
        // untuk effect tampilan
        cell.actualBlurView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.20)
        
        let blurFx = UIBlurEffect(style: .regular)
        let blurFxVw = UIVisualEffectView(effect: blurFx)
        blurFxVw.frame = cell.actualBlurView.bounds
        blurFxVw.alpha = 0.85
        cell.actualBlurView.addSubview(blurFxVw)
        
        // untuk populate data
        
        cell.articleImageOutlet.image = UIImage(named: articleContent.articleImageName)
        
        if articleContent.articleType == "chat" {
            cell.articleTypeLabel.text = "Interactive Dialogue"
            cell.articleBtn.setTitle("Open", for: .disabled)
        } else if articleContent.articleType == "free" {
            cell.articleTypeLabel.text = "Article"
            cell.articleBtn.setTitle("Read", for: .disabled)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let detailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "articleRead") as? AltArticleReadView {
//
//            detailView.modalPresentationStyle = .overCurrentContext
//
//            present(detailView, animated: true, completion: nil)
//        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // Get tapped cell location
        let cell = articleCollectionList.cellForItem(at: indexPath) as! CardCollectionViewCell
        cell.settings.cardContainerInsets = UIEdgeInsets(top: 8.0, left: 16.0, bottom: 8.0, right: 16.0)
        cell.settings.isEnabledBottomClose = false
        
        if article[indexPath.row].articleType == "free" {
            let viewController = storyboard.instantiateViewController(withIdentifier: "articleRead") as! AltArticleReadView
            
            transition = CardTransition(cell: cell, settings: cell.settings)
            viewController.settings = cell.settings
            viewController.transitioningDelegate = transition
            
            viewController.articleId = article[indexPath.row].articleId
            viewController.articleImgName = article[indexPath.row].articleImageName
            
            
            // If `modalPresentationStyle` is not `.fullScreen`, this should be set to true to make status bar depends on presented vc.
            //viewController.modalPresentationCapturesStatusBarAppearance = true
            viewController.modalPresentationStyle = .custom
            
            presentExpansion(viewController, cell: cell, animated: true)
        } else if article[indexPath.row].articleType == "chat" {
            let viewController = storyboard.instantiateViewController(withIdentifier: "chatRoomRead") as! AltDialogueView
            
            viewController.modalPresentationStyle = .fullScreen
            
            present(viewController, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AltArticleView: CardsViewController {
    
}
