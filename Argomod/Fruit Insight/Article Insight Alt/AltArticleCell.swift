//
//  AltArticleCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import AppstoreTransition

class AltArticleCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var articleImageOutlet: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var actualBlurView: UIImageView!
    @IBOutlet weak var articleTypeLabel: UILabel!
    @IBOutlet weak var articleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Make it appears very responsive to touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animate(isHighlighted: true)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animate(isHighlighted: false)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animate(isHighlighted: false)
    }
}

extension AltArticleCell: CardCollectionViewCell {
    
    var cardContentView: UIView {
        get {
            return containerView
        }
    }
    
}

