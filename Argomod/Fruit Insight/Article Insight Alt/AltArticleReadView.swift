//
//  AltArticleReadView.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import AppstoreTransition

class AltArticleReadView: UIViewController, UIScrollViewDelegate {
    
    // NOTE: AKTIFKAN CONSTRAINT HEIGHT PD ARTICLE OVERALL VIEW
    // AGAR BISA SCROLLING DLM STORYBOARD
    
    // MARK: - Outlet dan Variable
    
    @IBOutlet weak var articleOverallView: UIView!
    @IBOutlet weak var articleHeaderView: UIView!
    @IBOutlet weak var articleImageHeader: UIImageView!
    @IBOutlet weak var articleContentView: UIView!
    @IBOutlet weak var articleContentScrollView: UIScrollView!
    @IBOutlet weak var articleTextView: UITextView!
    
    @IBOutlet weak var libraryCollectionView: UICollectionView!
    @IBOutlet weak var startScanBtn: UIButton!
    @IBOutlet weak var textViewRightConstraint: NSLayoutConstraint!
    
    var articleId = Int()
    var articleImgName = String()
    
    var library: [LibraryArticle] = []
    var content: [ContentArticle] = []
    
    // MARK: - Inisialisasi
    
    override func viewDidLoad() {
       super.viewDidLoad()

       // Do any additional setup after loading the view.
        view.clipsToBounds = true
        articleContentScrollView.delegate = self
        
        libraryCollectionView.delegate = self
        libraryCollectionView.dataSource = self
        
        let _ = dismissHandler
        
        articleImageHeader.image = UIImage(named: "inArticle-\(articleImgName)")
        articleTextView.attributedText = processedArticleText()
        //articleTextView.translatesAutoresizingMaskIntoConstraints = true
        articleTextView.sizeToFit()
        articleTextView.isScrollEnabled = false
        articleTextView.adjustsFontForContentSizeCategory = true
        
        if #available(iOS 13.0, *) {
            articleTextView.textColor = UIColor.label
        } else {
            // None
        }
        
        processedLibrary()
    }
    
    // MARK: - Function Generate Text Artikel
    
    func processedArticleText() -> NSAttributedString {
        let processedString = NSMutableAttributedString()
        let processedParagraphStyle = NSMutableParagraphStyle()
        processedParagraphStyle.lineSpacing = 8
        let standardAttribute = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body), NSAttributedString.Key.paragraphStyle: processedParagraphStyle]
        let boldAttribute = [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body).bold(), NSAttributedString.Key.paragraphStyle: processedParagraphStyle]
        
        if articleId == 1 {
            
            let para1 = "Broccoli is known to be a hearty and tasty vegetable which is rich in dozens of nutrients.\n\n"
            let para2 = "It is said to pack the most nutritional punch of any vegetable. When we think about green vegetables to include in our diet, broccoli is one of the foremost veggies to come to our mind. Coming from the cabbage family, broccoli can be categorized as an edible green plant.\n\n"
            let para3 = "Here are some benefits of broccoli: \n\n"
            let para4 = "1. Cancer prevention\n2. Cholesterol reduction\n3. Reducing allergic reaction and inflammation\n4. Powerful antioxidant\n5. Bone health\n6. Hearth health\n7. Diet aid\n8. Great for detoxification\n9. Skin care\n10. Eye Care\n11. Anti - ageing\n"
            
            let para1PRCSED = NSAttributedString(string: para1, attributes: boldAttribute)
            let para2PRCSED = NSAttributedString(string: para2, attributes: standardAttribute)
            let para3PRCSED = NSAttributedString(string: para3, attributes: boldAttribute)
            let para4PRCSED = NSAttributedString(string: para4, attributes: standardAttribute)
            
            processedString.append(para1PRCSED)
            processedString.append(para2PRCSED)
            processedString.append(para3PRCSED)
            processedString.append(para4PRCSED)
            
        } else if articleId == 2 {
            
            let para1 = "Ginger is a popular ingredient in cooking, and especially in Asian and Indian cuisine. It has also been used for thousands of years for medicinal purposes.\n\n"
            let para2 = "The root or underground stem (rhizome) of the ginger plant can be consumed fresh, powdered, dried as a spice, in oil form, or as juice. Ginger is part of the Zingiberaceae family, alongside cardamom and turmeric. It is commonly produced in India, Jamaica, Fiji, Indonesia, and Australia.\n\n"
            let para3 = "It is available fresh and dried, as ginger extract and ginger oil, and in tinctures, capsules, and lozenges. Foods that contain ginger include gingerbread, cookies, ginger snaps, ginger ale, and a wide variety of savory recipes.\n\n"
            let para4 = "Here are some benefits of ginger: \n"
            let para5 = "1. Digestion\n2. Nausea\n3. Cold and flu relief\n4. Pain reduction\n5. Inflammation\n6. Cardiovascular health\n"
            
            let para1PRCSED = NSAttributedString(string: para1, attributes: boldAttribute)
            let para2PRCSED = NSAttributedString(string: para2, attributes: standardAttribute)
            let para3PRCSED = NSAttributedString(string: para3, attributes: standardAttribute)
            let para4PRCSED = NSAttributedString(string: para4, attributes: boldAttribute)
            let para5PRCSED = NSAttributedString(string: para5, attributes: standardAttribute)
            
            processedString.append(para1PRCSED)
            processedString.append(para2PRCSED)
            processedString.append(para3PRCSED)
            processedString.append(para4PRCSED)
            processedString.append(para5PRCSED)
            
        } else if articleId == 3 {
            
            let para1 = "Citrus fruit include oranges, lemons, limes and grapefruits, in addition to tangerines and pomelos.\n\n"
            let para2 = "Citrus flavonoids are also antioxidants that can neutralize free radicals and may protect against heart disease. Studies show that citrus flavonoids may improve blood flow through coronary arteries, reduce the ability of arteries to form blood clots and prevent the oxidation of LDL (“bad”) cholesterol, which is an initial step in the formation of artery plaques.\n\n"
            let para3 = "Citrus fruits are also high in vitamin C, and are good sources of folate and thiamin. Vitamin C is a powerful antioxidant and protects the body from damaging free radicals2. It is also required for the synthesis of collagen, which helps wounds heal and helps hold blood vessels, tendons, ligaments and bone together.\n\n"
            let para4 = "Did you Know?\n\n"
            let para5 = "The vitamin C in citrus fruit strongly enhances the absorption of iron in food. Vitamin C binds to iron in the digestive tract and the iron-vitamin C complex is absorbed together.\n\n"
            let para6 = "How to keep citrus fruits\n\n"
            let para7 = "1. Store citrus fruit at room temperature if you'll eat it in a week or so; otherwise, it will keep in the crisper for six to eight weeks.\n\n2. Squirt some lemon juice on fresh cut fruits or fresh guacamole to prevent them from browning quickly.\n"
            
            let para1PRCSED = NSAttributedString(string: para1, attributes: boldAttribute)
            let para2PRCSED = NSAttributedString(string: para2, attributes: standardAttribute)
            let para3PRCSED = NSAttributedString(string: para3, attributes: standardAttribute)
            let para4PRCSED = NSAttributedString(string: para4, attributes: boldAttribute)
            let para5PRCSED = NSAttributedString(string: para5, attributes: standardAttribute)
            let para6PRCSED = NSAttributedString(string: para6, attributes: boldAttribute)
            let para7PRCSED = NSAttributedString(string: para7, attributes: standardAttribute)
            
            processedString.append(para1PRCSED)
            processedString.append(para2PRCSED)
            processedString.append(para3PRCSED)
            processedString.append(para4PRCSED)
            processedString.append(para5PRCSED)
            processedString.append(para6PRCSED)
            processedString.append(para7PRCSED)
            
        }
        
        return processedString
    }
    
    func processedLibrary() {
        if articleId == 1 {
            let lib1 = LibraryArticle(articleId: 0, articleImgName: "broccoli", articleIngridientLabel: "Broccoli")
            
            library.append(lib1)
            
            startScanBtn.setTitle("Start scan a broccoli now", for: .normal)
        } else if articleId == 2 {
            let lib1 = LibraryArticle(articleId: 2, articleImgName: "ginger", articleIngridientLabel: "Ginger")
            
            library.append(lib1)
            
            startScanBtn.setTitle("Start scan a ginger now", for: .normal)
        } else if articleId == 3 {
            let lib1 = LibraryArticle(articleId: 1, articleImgName: "lemon", articleIngridientLabel: "Lemon")
            let lib2 = LibraryArticle(articleId: 1, articleImgName: "lime", articleIngridientLabel: "Lime")
            let lib3 = LibraryArticle(articleId: 1, articleImgName: "orange", articleIngridientLabel: "Orange")
            
            library += [lib1, lib2, lib3]
            
            startScanBtn.setTitle("Start scan citrus now", for: .normal)
        }
    }
    
    // MARK: - Function lain-lain
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // prevent bouncing when swiping down to close
        scrollView.bounces = scrollView.contentOffset.y > 100
        
        dismissHandler.scrollViewDidScroll(scrollView)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func dismissArticle(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
}

// MARK: - Function Libraru Collection

extension AltArticleReadView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return library.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "libraryCell", for: indexPath) as! LibraryCell
        
        // untuk konfigurasi tampilan
        cell.contentView.layer.cornerRadius = 5.0
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.clipsToBounds = true
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        // untuk populate data
        let content = library[indexPath.row]
        cell.libraryImage.image = UIImage(named: content.articleImgName)
        cell.libraryName.text = content.articleIngridientLabel
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = UIScreen.main.bounds.width
        
        if screenWidth > 375.5 {
            return CGSize(width: 165, height: 165)
        } else {
            return CGSize(width: 145, height: 145)
        }
        
    }
}

extension UIFont {
    func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}

extension AltArticleReadView: CardDetailViewController {
    
    var scrollView: UIScrollView {
        return articleContentScrollView
    }
    
    
    var cardContentView: UIView {
        return articleHeaderView
    }

}
