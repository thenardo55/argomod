//
//  AltArticle.swift
//  Argomod
//
//  Created by Thenardo Ardo on 10/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class AltArticle {
    var articleId: Int
    var articleTitle: String
    var articleType: String
    var articleImageName: String
    
    init(articleId: Int, articleTitle: String, articleType: String, articleImageName: String) {
        self.articleId = articleId
        self.articleTitle = articleTitle
        self.articleType = articleType
        self.articleImageName = articleImageName
    }
}
