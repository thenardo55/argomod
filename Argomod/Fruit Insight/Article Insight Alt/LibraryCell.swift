//
//  LibraryCell.swift
//  Argomod
//
//  Created by Thenardo Ardo on 18/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class LibraryCell: UICollectionViewCell {
    
    @IBOutlet weak var libraryImage: UIImageView!
    @IBOutlet weak var libraryName: UILabel!
}
