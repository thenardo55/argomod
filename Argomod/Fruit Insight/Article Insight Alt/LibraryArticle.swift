//
//  LibraryArticle.swift
//  Argomod
//
//  Created by Thenardo Ardo on 18/10/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class LibraryArticle {
    var articleId: Int
    var articleImgName: String
    var articleIngridientLabel: String
    
    init(articleId: Int, articleImgName: String, articleIngridientLabel: String) {
        self.articleId = articleId
        self.articleImgName = articleImgName
        self.articleIngridientLabel = articleIngridientLabel
    }
}
