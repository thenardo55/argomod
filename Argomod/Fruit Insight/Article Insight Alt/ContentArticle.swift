//
//  ContentArticle.swift
//  Argomod
//
//  Created by Thenardo Ardo on 21/11/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class ContentArticle {
    var articleId: String
    var articleText: String
    var textFormat: String
    
    init(id: String, text: String, format: String) {
        self.articleId = id
        self.articleText = text
        self.textFormat = format
    }
}
